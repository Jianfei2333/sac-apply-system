create table userList (
  userId char(8),
  userName varchar(10),
  userType enum('normalUser', 'assistant', 'admin', 'association'),
  password char(32),
  email varchar(40),
  phone char(13),
  gender enum('male', 'female'),
  registerDateTime char(19),
  lastLoginDateTime char(19),
  activeState tinyint(1),
  activeCode char(8),
  primary key (userId)
)engine=InnoDB, default charset=utf8;

create table assistantList (
  assistantId char(8),
  workingHours int,
  voluntererHours int,
  workingHoursUnsettled int,
  primary key (assistantId)
)engine=InnoDB, default charset=utf8;

create table orgList (
  orgId char(6),
  guide varchar(20),
  orgName varchar(20),
  contactId char(8),
  primary key (orgId)
)engine=InnoDB, default charset=utf8;

create table settingsList (
  settingId int auto_increment,
  description varchar(100),
  primary key (settingId)
)engine=InnoDB, default charset=utf8;

create table userSettingsList (
  userId char(8),
  settingString varchar(20),
  primary key (userId)
)engine=InnoDB, default charset=utf8;

create table blockList (
  seq int auto_increment,
  id varchar(8),
  insertDate varchar(10),
  removeDate varchar(10),
  applyId int,
  primary key (seq)
)engine=InnoDB, default charset=utf8;

create table roomList (
  roomId varchar(5),
  roomName varchar(10),
  roomType enum('gq', 'tl', 'wh', 'others'),
  primary key (roomId)
)engine=InnoDB, default charset=utf8;

create table applyList (
  applyId int auto_increment,
  userId char(8),
  unit enum('ind', 'org'),
  orgId char(6),
  roomId varchar(5),
  useDate char(10),
  startTime char(8),
  endTime char(8),
  submitDateTime char(19),
  reason varchar(300),
  status enum('pending','pass', 'reject', 'using', 'violate', 'finish'),
  assistantId char(8),
  feedback varchar(100),
  primary key (applyId)
)engine=InnoDB, default charset=utf8;

create table noticeList (
  seq int auto_increment,
  noticeType enum('static', 'popup'),
  topic varchar(30),
  content varchar(500),
  submitDate char(10),
  endDate char(10),
  primary key (seq)
)engine=InnoDB, default charset=utf8;

create table fileList (
  seq int auto_increment,
  fileName varchar(50),
  updateDate char(10),
  valid tinyint(1),
  primary key (seq)
)engine=InnoDB, default charset=utf8;

create table warehousePermissionList (
  orgId char(6),
  roomId varchar(5),
  violateTimes int,
  primary key (orgId, roomId)
)engine=InnoDB, default charset=utf8;

create table adviceList (
  userId char(8),
  submitDateTime char(19),
  content varchar(500)
)engine=InnoDB, default charset=utf8;

create table longtermApplyList (
  orgId char(6),
  roomId varchar(5),
  submitDateTime char(19),
  useDay tinyint(1),
  startTime char(8),
  endTime char(8),
  reason varchar(500),
  status enum('pending', 'accept', 'reject'),
  feedback varchar(100)
)engine=InnoDB, default charset=utf8;

create table workTable (
  day tinyint(1),
  duty tinyint(1),
  assistantId char(8),
  actualId char(8),
  sign tinyint(1)
)engine=InnoDB, default charset=utf8;

create table signUpList (
  assistantId char(8),
  Mon varchar(5),
  Tue varchar(5),
  Web varchar(5),
  Thu varchar(5),
  Fri varchar(5),
  Sat varchar(5),
  Sun varchar(5)
)engine=InnoDB, default charset=utf8;

create table loginLog (
  userId char(8),
  loginDateTime char(19),
  IPAddress varchar(15)
)engine=InnoDB, default charset=utf8;

create table warehouseLog (
  roomId varchar(5),
  useDate varchar(10),
  startTime varchar(8),
  endTime varchar(8),
  violate tinyint(1),
  assistantId char(8),
  reason varchar(100)
)engine=InnoDB, default charset=utf8;

create table workLog (
  workDate char(10),
  workDay tinyint(1),
  duty tinyint(1),
  assistantId char(8)
)engine=InnoDB, default charset=utf8;
