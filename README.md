# sac-project

> sac apply system

## Build Setup

``` bash
# install dependencies
npm install

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test

# project test with hot loading
gulp full-stack

# build for production (back end)
gulp transmit-file
```

## File tree

```
 .
 ├─── build/                                # webpack with config files
 │
 ├─── config/
 │    ├── index.js                          # main project config file
 │    └── ...
 │
 ├─── dist/                                 # target path
 │    ├── front_end/
 │    └── back_end/
 │
 ├─── src/                                  # source file path
 │    ├── front_end/
 │    │   ├── assets/                       # assets like 'logo.png', 'style.less'
 │    │   ├── components/                   # page ui components
 │    │   │   ├── some_page/
 │    │   │   │   ├── some_page.vue
 │    │   │   │   ├── some_page.js
 │    │   │   │   └── some_page.css
 │    │   │   └── ...
 │    │   ├── router/                       # front end router
 │    │   │   ├── index.js                  
 │    │   │   └── routes.js                 # router config file
 │    │   ├── store/
 │    │   │   └── store.js                  # vue-store file
 │    │   ├── App.vue                       # main App component
 │    │   └── main.js                       # app entry file
 │    └── back_end/ 
 │        ├── config/                       # config
 │        ├── controllers/                  # controller
 │        ├── models/                       # model
 │        ├── public/                       # back end static folder
 │        ├── routes/                       # router
 │        ├── utils/                        # core functional scripts
 │        ├── scripts/                      # additional functional scripts
 │        └── app.js                        # back end app entry
 │
 ├─── static/                               # static file deposit, created by system
 ├─── test/                                 # Jest testing files
 ├─── .babelrc                              # babel config
 ├─── .editorconfig                         # additional config for editor
 ├─── .eslintignore                         # eslint ignore rules
 ├─── .eslintrc.js                          # eslint config 
 ├─── .gitignore                            # git ignore rules
 ├─── .postcssrc.js                         # postcss config
 ├─── gulpfile.js                           # gulp config
 ├─── index.html                            # html template
 ├─── package.json                          # build scripts and dependencies
 ├─── README.md*                            # this file
 └─── sac.sql                               # database schema
```


For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
