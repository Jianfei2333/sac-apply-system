import HelloWorld from '../components/HelloWorld/HelloWorld.vue'
import LoginRegister from '../components/LoginRegister/LoginRegister.vue'
import User from '../components/User/User.vue'

export default {
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login-register',
      name: 'LoginRegister',
      component: LoginRegister
    },
    {
      path: '/user',
      name: 'User',
      component: User
    }
  ]
}
