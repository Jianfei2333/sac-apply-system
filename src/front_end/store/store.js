import Vue from 'vue'
import Vuex from 'vuex'
import { Message } from 'element-ui'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    UserStatus: 'guest',
    apiUrl: '/api',
    UserInfo: {},
    routerTable: {
      'guest': '/login-register',
      'normalUser': '/user'
    }
  },
  mutations: {
    resetState (state) {
      state.UserStatus = 'guest'
      state.UserInfo = {}
    },
    updateUserStatus (state, result) {
      state.UserStatus = result
    },
    updateUserInfo (state, _info) {
      state.UserInfo = _info
    },
    /*
     * @author: Jianfei2333
     * @email: xjf999999@hotmail.com
     * @function: 统一风格操作反馈
     * 使用方法： this.$store.commit('info', info_to_display)
     * info_to_display结构：
     * {
     *  msg: 反馈框内容
     *  type: 反馈框类型，分为info,warning,success
     * }
     */
    info (state, data) {
      Message({
        showClose: true,
        message: data.msg,
        type: data.type,
        duration: 1200
      })
    },
    error (state, err) {
      console.log(err)
      Message({
        showClose: true,
        message: '遇到了错误，请联系管理员！',
        type: 'error',
        duration: 1200
      })
    }
  },
  actions: {
    updateUserStatus ({state, commit}) {
      return Vue.http.get(state.apiUrl + '/user/checkSession')
        .then((res) => {
          if (res.body.status === 'FAIL') {
            commit('info', {
              msg: '请登陆',
              type: 'info'
            })
          } else if (res.body.status === 'ERR') {
            throw new Error()
          } else {
            commit('info', {
              msg: '登陆成功',
              type: 'success'
            })
            commit('updateUserStatus', res.body.content)
          }
          if (res.body.content === undefined) {
            return res.body.content
          } else {
            return res.body.content.userType
          }
        })
        .catch((err) => {
          commit('error', err)
        })
    },
    updateUserInfo ({state, commit}, info) {
      // console.log(info)
      commit('updateUserInfo', info)
      commit('updateUserStatus', info.userType)
    }
  },
  methods: {

  }
})
