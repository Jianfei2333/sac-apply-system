export default {
  name: 'User',
  data () {
    return {
      msg: 'this is User'
    }
  },
  beforeMount() {
    if (this.$store.state.UserStatus !== 'normalUser') {
      this.$router.replace('/login-register')
      this.$store.commit('info', {
        msg: '重定向到登陆页'
      })
    }
  },
  methods: {
    logout: function () {
      this.$http.get('/api/user/logout')
        .then((res) => {
          if (res.body.status === "SUCCESS") {
            this.$store.commit('info', {
              msg: '登出成功',
              type: 'success'
            })
            this.$router.replace('/login-register')
          } else if (res.body.status === "FAIL") {
            this.$store.commit('info', {
              msg: '出现了问题，请稍候重试',
              type: 'warning'
            })
          } else {
            this.$store.commit('error')
          }
        })
    }
  }
}
