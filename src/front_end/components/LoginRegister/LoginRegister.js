const md5 = require('md5')
const _ = require('lodash')

export default {
  name: 'LoginRegister',
  data () {
    // 注册表单，可用性检测：重复密码与密码一致性
    var passCheck = (rule, value, callback) => {
      if (value === '') {
        callback(new Error('请重复密码'))
      } else if (value !== this.registerForm.password) {
        callback(new Error('两次输入密码不一致'))
      } else {
        callback()
      }
    }

    return {
      msg: 'this is LoginRegister',
      dialogRegisterVisible: false,
      dialogActiveVisible: false,
      registerForm: {
        userId: '15336204',
        userName: '邢剑飞',
        password: '123456',
        password_check: '123456',
        email: 'xingjf3',
        phone: '15521030152',
        gender: 'male'
      },
      registerRules: {
        userId: [{ 
          type:"string",
          pattern: /^[0-9]{8}$/,
          required: true,
          message: '请输入正确的学号',
          trigger: 'blur'
        }],
        userName: [{
          required: true,
          message: '请输入姓名',
          trigger: 'blur'
        }],
        email: [{
          type: 'string',
          required: true,
          pattern: /^[0-9a-zA-Z]+$/,
          message: '请输入正确的邮箱',
          trigger: 'blur'
        }],
        password: [{
          required: true,
          message: '请输入6-18位密码',
          trigger: 'blur',
          min: 6,
          max: 18
        }],
        password_check: [{
          required: true,
          validator: passCheck,
          trigger: 'blur'
        }],
        phone: [{
          type: 'string',
          pattern: /^1[0-9]{10}$/,
          required: true,
          message: '请输入正确的手机号码',
          trigger: 'blur'
        }],
        gender: [{
          required: true,
          message: '请选择',
          trigger: 'blur'
        }]
      },
      loginForm: {
        userId: '',
        password: ''
      },
      activeForm: {
        activeCode: ''
      }
    }
  },
  methods: {
    resetForm(formName) {
      this.$refs[formName].resetFields()
    },
    submitRegister: function () {
      this.$refs['registerForm'].validate((valid) => {
        if (valid) {
          this.dialogRegisterVisible=false
          // 数据处理
          this.registerForm.email = this.registerForm.email+'@mail2.sysu.edu.cn'
          this.registerForm.password = md5(this.registerForm.password)
          this.registerForm.password_check = null
          // 提交注册
          this.$http.post('/api/user/register', this.registerForm)
            .then((res) => {
              // console.log(res.body)
              if (res.body.status === 'FAIL') {
                this.$store.commit('info', {
                  msg: "该用户已被注册，请检查信息后重试，或使用找回密码功能",
                  type: "warning"
                })
              } else if (res.body.status === 'SUCCESS') {
                this.$store.commit('info', {
                  msg: '注册成功,已发送激活邮件，请检查邮箱',
                  type: 'success'
                })
                this.dialogActiveVisible = true
              } else {
                this.$store.commit('error')
              }
              this.resetForm('registerForm')
            })
        } else {
          this.$store.commit('info', {
            msg: '注册表单内容存在错误',
            type: 'warning'
          })
        }
      })
    },
    submitLogin: function() {
      this.$http.post('/api/user/login', {
        userId: this.loginForm.userId,
        password: md5(this.loginForm.password)
      })
        .then((res) => {
          // console.log(res.body)
          if (res.body.status === 'FAIL' && res.body.description === 'PASSWORD_INCORRECT') {
            this.$store.commit('info', {
              msg: '用户名或密码错误',
              type: 'warning'
            })
          } else if (res.body.status === 'FAIL' && res.body.description === 'NON_ACTIVE_STATE') {
            this.$store.commit('info', {
              msg: '帐号未激活，请检查邮箱中的激活邮件',
              type: 'warning'
            })
            this.dialogActiveVisible = true
          } else if (res.body.status === 'ERROR') {
            this.$store.commit('error')
          } else {
            this.$store.dispatch('updateUserInfo', res.body.content)
            this.$router.replace(this.$store.state.routerTable[res.body.content.userType])
          }
        })
    },
    submitActive: function () {
      this.$http.post('/api/user/active', {
        userId: this.loginForm.userId,
        password: md5(this.loginForm.password),
        activeCode: this.activeForm.activeCode
      })
        .then((res) => {
          // console.log(res.body)
          if (res.body.status === 'FAIL') {
            if (res.body.description === 'PASSWORD_INCORRECT') {
              this.$store.commit('info', {
                msg: '用户名或密码错误',
                type: 'warning'
              })
            } else if (res.body.description === 'ACTIVE_CODE_INCORRECT') {
              this.$store.commit('info', {
                msg: '激活码错误',
                type: 'warning'
              })
            } else if (res.body.description === 'ACTIVE_CODE_TIMEOUT') {
              this.$store.commit('info', {
                msg: '激活码超时，请重新获取',
                type: 'warning'
              })
            }
          } else if (res.body.status === 'ERROR') {
            this.$store.commit('error')
          } else {
            this.$store.dispatch('updateUserInfo', res.body.content)
            this.$router.replace(this.$store.state.routerTable[res.body.content.userType])
          }
        })
    },
    resendActiveEmail: function () {
      this.$http.post('/api/user/resend-active-email', {
        userId: this.loginForm.userId,
        password: md5(this.loginForm.password)
      })
        .then((res) => {
          // console.log(res.body)
          if (res.body.status === 'SUCCESS') {
            this.$store.commit('info', {
              msg: '邮件已发送，请检查邮箱',
              type: 'success'
            })
          }
        })
    }
  }
}
