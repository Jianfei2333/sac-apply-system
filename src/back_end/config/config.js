const _HOST = 'localhost'
const _USER = 'root'
const _PASS = 'sysu_sac_mysql'
const _DB = 'sacdata'

module.exports = {
  salt: 'SYSU_SAC',
  db_config: {
    host: _HOST,
    user: _USER,
    password: _PASS,
    database: _DB
  },
  session_schema: {
    name: 'sac-session-id',
    saveUninitialized: true,
    resave: true,
    secret: 'SAC_NEW_APPLY_SYSTEM',
    unset: 'destroy',
    cookie: {
      path: '/',
      httpOnly: true,
      secure: false,
      maxAge: 24 * 60 * 60 * 1000
    }
  },
  session_config: {
    host: _HOST,
    'user': _USER,
    password: _PASS,
    database: _DB,
    schema: {
      tableName: 'sessions',
      columnNames: {
        session_id: 'sid',
        expires: 'expires',
        data: 'session'
      }
    }
  },
  mailer_config: {
    host: "smtp.mxhichina.com",
    secureConnection: true,
    port: 465,
    auth: {
      user: 'notice@sysu-sac.com',
      pass: 'Nhdzx_mail'
    }
  }
}
