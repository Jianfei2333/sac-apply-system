const express = require('express')
var app = express()

const session = require('express-session')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const MySQLStore = require('express-mysql-session')(session)

const config = require('./config/config.js')
const resBody = require('./utils/resBody.js')
const User = require('./routes/user.js')

// favourite icon setting
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

// session配置

const sc = config.session_schema
sc.store = new MySQLStore(config.session_config)
app.use(session(sc))

// 静态资源路径

app.use(express.static(path.join(__dirname, '/../front_end')))

// 路由中间件

app.use('/api/user', User)

app.use((req, res, next) => {
  if (!req.session.user) {
    resBody.fail(res, 'NOT_LOGIN')
  } else {
    next()
  }
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
