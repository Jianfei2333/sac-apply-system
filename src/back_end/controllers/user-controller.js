const resBody = require('../utils/resBody.js')
const UserModel = require('../models/user-model.js')
const mailer = require('../utils/mailer')
const config = require('../config/config')
const _ = require('lodash')
const md5 = require('md5')
const moment = require('moment')

/*
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 下面为Session检查api,用户每次打开网页时请求
 */

exports.checkSession = (req, res, next) => {
  console.log(req.session.user)
  if (req.session.user) {
    console.log('hahaha')
  }
  if (req.session.user) {
    resBody.success(res, 'CHECK_SESSION_SUCCESS', req.session.user)
  } else {
    resBody.fail(res, 'NO_SESSION_INFO')
  }
}

/*
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 下面部分为注册api，未登录状态请求
 */

exports.register = async (req, res, next) => {
  console.log(req.body)
  
  // 获取连接对端IP
  var ip = req.headers['x-forwarded-for'] ||
      req.ip ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      req.connection.socket.remoteAddress || ''
  if(ip.split(',').length > 0) {
      ip = ip.split(',')[0]; // 取第一个ip即可。
      ip = ip.split(':')[3]; // ip为：：ffff:127.0.0.1这样的字串，这是IPV6格式的地址，取最后一项。
  }

  let preCheck = await checkConflict(req.body) && checkValidation(req.body)

  if (preCheck) {
    let activeCode = generateActiveCode(req.body.password)
    UserModel.addUser(_.assign(req.body, {'activeCode': activeCode}))
      .then(async (d) => {
        return await mailCheck(req.body, activeCode)
      })
      .then(() => {
        resBody.success(res, 'REGISTER_SUCCESS')
      })
      .catch((err) => {
        resBody.error(res, err)
      })
  } else {
    resBody.fail(res, 'CHECK_FAIL')
  }
}

/*
 * 冲突检查：检查用户提交的注册信息是否已经存在于数据库
 */
async function checkConflict(body) {
  let check = await UserModel.checkUser(body)
  return check.length === 0
}

/*
 * 可用性检查：检查email是否为合法中大邮箱，学号是否符合要求,电话是否符合要求
 */
function checkValidation(body) {
  // 邮箱
  let patt1 = new RegExp("^[0-9a-z]+@mail2.sysu.edu.cn$")
  let ch1 = patt1.test(body.email)
  // 学号
  let patt2 = new RegExp("^[0-9]{8}$")
  let ch2 = patt2.test(body.userId)
  // 电话
  let patt3 = new RegExp("^[0-9]{11}$")
  let ch3 = patt3.test(body.phone)
  return ch1 && ch2 && ch3
}

/*
 * 发送验证邮件
 */
async function mailCheck(body, activeCode) {
  try{
    mailer.checkEmail(body.email, {
      name: body.userName,
      sid: body.userId,
      code: activeCode
    })
  } catch (err) {
    throw new Error(err)
  }
}

/*
 * 生成校验码
 */
function generateActiveCode(password) {
  gcode = md5(password.substr(0, 4)+config.salt)
  return gcode.substr(0,8)
}

/* * * 注册部分组件完毕 * * */

/*
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 下面部分为登陆api，未登录状态请求
 */

exports.login = async (req, res, next) => {
  console.log(req.body)
  let data = await UserModel.getUser(req.body)
  if ((data.length !== 0 && data[0].password !== req.body.password) || data.length === 0) {
    resBody.fail(res, 'PASSWORD_INCORRECT')
  } else if (data[0].activeState === 0) {
    resBody.fail(res, 'NON_ACTIVE_STATE')
  } else {
    req.session.user = data[0]
    resBody.success(res, 'LOGIN_SUCCESS', data[0])
  }
}

/*
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 下面为激活api,未登录状态请求
 */

exports.active = async (req, res, next) => {
  let data = await UserModel.getUser(req.body)
  if (data[0].password !== req.body.password) {
    resBody.fail(res, 'PASSWORD_INCORRECT')
  } else if (data[0].activeCode !== req.body.activeCode) {
    resBody.fail(res, 'ACTIVE_CODE_INCORRECT')
  } else if (moment().subtract(12, 'hours') > moment(data[0].registerDateTime)) {
    resBody.fail(res, 'ACTIVE_CODE_TIMEOUT')
  } else {
    UserModel.updateActiveState(req.body)
      .then((d) => {
        req.session.user = data[0]
        resBody.success(res, 'LOGIN_SUCCESS', data[0])
      })
  }
}

/*
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 下面为发送激活邮件api,会同时更新注册时间
 */

exports.resendActiveEmail = async (req, res, next) => {
  console.log(req.body)
  let data = await UserModel.getUser(req.body)
  let newActiveCode = generateActiveCode(data[0].activeCode)
  UserModel.updateActiveCode({
    activeCode: newActiveCode,
    registerDateTime: moment().format('YYYY-MM-DD HH:mm:ss'),
    userId: req.body.userId
  })
    .then(async (d) => {
      console.log(data[0])
      return await mailCheck(data[0], newActiveCode)
    })
    .then((d) => {
      resBody.success(res, 'SUCCESS_UPDATE_ACTIVE_CODE')
    })
    .catch((err) => {
      resBody.error(res, err)
    })
}

exports.logout = (req, res, next) => {
  req.session.user = null
  resBody.success(res, 'SUCCESS_LOG_OUT')
}

/*
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 下面为测试Url,检查链接可用性
 */

exports.testUrl = (req, res, next) => {
  console.log(req.body)
  resBody.success(res, 'CONNECTION_VALID')
}