const { queryDb } = require('../utils/dbConn')
const moment = require('moment')

exports.checkUser = (data) => {
  console.log(data)
  let query = 
    "select * from userList \n" +
    "where userId = ? \n" +
    "or email = ? \n" +
    "or phone = ? \n" +
    ";"
  let values = [data.userId, data.email, data.phone]
  return queryDb(query, values)
}

exports.addUser = (data) => {
  console.log(data)
  let query = 
    "insert into userList \n" +
    "(userId, userName, userType, password, email, phone, \n" +
    "gender, registerDateTime, activeState, activeCode) values \n" +
    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) \n" +
    ";"
  let values = [data.userId, data.userName, 'normalUser', data.password, data.email,
                data.phone, data.gender, moment().format('YYYY-MM-DD HH:mm:ss'), '0',
                data.activeCode]
  return queryDb(query, values)
}

exports.getUser = async (data) => {
  let query =
    "select * from userList where userId = ? \n" +
    ";"
  return queryDb(query, [data.userId])
}

exports.updateActiveState = (data) => {
  console.log('call updateActiveState')
  let query = 
    "update userList set activeState = 0 where userId = ? \n" +
    ";"
  return queryDb(query, [data.userId])
}

exports.updateActiveCode = (data) => {
  let query =
    "update userList set activeCode = ?, registerDateTime = ? where userId = ? \n" +
    ";"
  let values = [data.activeCode, data.registerDateTime, data.userId]
  return queryDb(query, values)
}