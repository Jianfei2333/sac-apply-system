const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user-controller.js')

/*
 * GET
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 检查Session信息
 */
router.get('/checkSession', UserController.checkSession)

/*
 * POST
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 提交注册
 */
router.post('/register', UserController.register)

/*
 * POST
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 提交登陆
 */
router.post('/login', UserController.login)

/*
 * POST
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 激活状态检查
 */
router.post('/active', UserController.active)

router.post('/resend-active-email', UserController.resendActiveEmail)

router.get('/logout', UserController.logout)

/*
 * GET
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: 测试
 */
router.get('/testUrl', UserController.testUrl)

module.exports = router
