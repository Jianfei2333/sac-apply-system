const config = require('../config/config')
const nodemailer = require('nodemailer')
const moment = require('moment')
const _ = require('lodash')

const transporter = nodemailer.createTransport(config.mailer_config)
function sendMail(to, title, content) {
  let option = {
    from: 'notice@sysu-sac.com',
    to,
    subject: title,
    html: content
  }
  return new Promise(function(resolve, reject) {
    transporter.sendMail(option, (err, res) => {
      if (err) reject(err)
      else resolve('mail sent')
    })
  })
}
module.exports = {
  /*
   * 发送自动审批结果
   * @method master
   * @param to <String> 收件人
   * @param option <Object> 选项
   *   - startTime <Date> 自动审批开始时间 default=new Date()
   *   - endTime <Date> 自动审批结束时间 default=new Date()
   *   - permit <Number> 审批通过数量 default=0
   *   - reject <Number> 审批未通过数量 default=0
   * @return <Promise>
   */
  master(to, option) {
      option = _.assign({
        startTime: new Date(),
        endTime: new Date(),
        permit: 0,
        reject: 0
      }, option)
      let title = '[SAC-自动审批结果]'
      let content =
        `<table align="center" border="1" cellpadding="20">
        <tr>
          <th>通过</th>
          <th>未通过</th>
          <th>开始时间</th>
          <th>结束时间</th>
        </tr>
        <tr>
          <td align="center">${option.permit}</td>
          <td align="center">${option.reject}</td>
          <td align="center">${moment(option.startTime).format('YYYY-MM-DD HH:mm')}</td>
          <td align="center">${moment(option.endTime).format('YYYY-MM-DD HH:mm')}</td>
        </tr>
      </table>`
      return sendMail(to, title, content)
    },
    /*
     * 发送验证码验证邮箱, 不限于注册
     * @method regist
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - code <String> 验证码 default=''
     * @return <Promise>
     */
    checkEmail(to, option) {
      option = _.assign({
        name: '',
        sid: '',
        code: '',
      }, option)
      option.sid = String(option.sid)
      let title = '[SAC - 邮箱验证]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
        您好！<br/><br/>
        您的验证码为 <big>${option.code}</big><br/><br/>
        切勿将验证码告知他人<br/><br/>
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
     * 提交申请的确认邮件
     * @method apply
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - applyID <String> 预约号 default=''
     *   - room <String> 房间名 default=''
     *   - unit <String> 使用单位 个人:'个人' 组织:'组织-组织名' default='个人'
     *   - startTime <Date> 开始时间 default=new Date()
     *   - endTime <Date> 结束时间 default=new Date()
     * @return <Promise>
     */
    apply(to, option) {
      option = _.assign({
        name: '',
        applyID: '',
        room: '',
        unit:'个人',
        startTime: new Date(),
        endTime: new Date()
      }, option)
      let title = '[SAC - 申请提交成功]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
        您好！<b>您的申请已成功提交</b>，请注意查收审近日邮件获悉审批结果。
        <p>
        <table width="870px" border="1" cellpadding="20">
          <tr>
            <th>预约号</th>
            <th>功能房</th>
            <th>使用单位</th>
            <th>使用日期</th>
            <th>开始时间</th>
            <th>结束时间</th>
            <th>审批状态</th>
          </tr>
          <tr>
            <td align="center" width="60px">${option.applyID}</td>
            <td align="center" width="120px">${option.room}</td>
            <td align="center" width="200px">${option.unit}</td>
            <td align="center" width="150px">${moment(option.startTime).format('YYYY-MM-DD')}</td>
            <td align="center" width="80px">${moment(option.startTime).format('HH:mm')}</td>
            <td align="center" width="80px">${moment(option.endTime).format('HH:mm')}</td>
            <td align="center" width="80px">审批中</td>
          </tr>
        </table>
        </p>
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
     * 取消申请的确认邮件
     * @method apply
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - applyID <String> 预约号 default=''
     *   - room <String> 房间名 default=''
     *   - unit <String> 使用单位 个人:'个人' 组织:'组织-组织名' default='个人'
     *   - startTime <Date> 开始时间 default=new Date()
     *   - endTime <Date> 结束时间 default=new Date()
     * @return <Promise>
     */
    cancel(to, option) {
      option = _.assign({
        name: '',
        applyID: '',
        room: '',
        unit:'个人',
        startTime: new Date(),
        endTime: new Date()
      }, option)
      let title = '[SAC - 您的申请已取消]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
        您好！<b>您的申请已取消</b>。<br/>
        <p>
        <table width="870px" border="1" cellpadding="20">
          <tr>
            <th>预约号</th>
            <th>功能房</th>
            <th>使用单位</th>
            <th>使用日期</th>
            <th>开始时间</th>
            <th>结束时间</th>
            <th>审批状态</th>
          </tr>
          <tr>
            <td align="center" width="60px">${option.applyID}</td>
            <td align="center" width="120px">${option.room}</td>
            <td align="center" width="200px">${option.unit}</td>
            <td align="center" width="150px">${moment(option.startTime).format('YYYY-MM-DD')}</td>
            <td align="center" width="80px">${moment(option.startTime).format('HH:mm')}</td>
            <td align="center" width="80px">${moment(option.endTime).format('HH:mm')}</td>
            <td align="center" width="80px">已取消</td>
          </tr>
        </table>
        </p>
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
     * 通过申请的通知邮件
     * @method apply
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - applyID <String> 预约号 default=''
     *   - room <String> 房间名 default=''
     *   - unit <String> 使用单位 个人:'个人' 组织:'组织-组织名' default='个人'
     *   - startTime <Date> 开始时间 default=new Date()
     *   - endTime <Date> 结束时间 default=new Date()
     * @return <Promise>
     */
    permit(to, option) {
      option = _.assign({
        name: '',
        applyID: '',
        room: '',
        unit:'个人',
        usedate: '',
        startTime: new Date(),
        endTime: new Date()
      }, option)
      let title = '[SAC - 您的申请已经通过]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
        您好！<b>您的申请已通过审批</b>。
        <p>
        <table width="870px" border="1" cellpadding="20">
          <tr>
            <th>预约号</th>
            <th>功能房</th>
            <th>使用单位</th>
            <th>使用日期</th>
            <th>开始时间</th>
            <th>结束时间</th>
            <th>审批状态</th>
          </tr>
          <tr>
            <td align="center" width="60px">${option.applyID}</td>
            <td align="center" width="120px">${option.room}</td>
            <td align="center" width="200px">${option.unit}</td>
            <td align="center" width="150px">${moment(option.usedate).format('YYYY-MM-DD')}</td>
            <td align="center" width="80px">${option.startTime}</td>
            <td align="center" width="80px">${option.endTime}</td>
            <td align="center" width="80px">通过</td>
          </tr>
        </table>
        </p>
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
     * 未通过申请的通知邮件
     * @method apply
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - applyID <String> 预约号 default=''
     *   - room <String> 房间名 default=''
     *   - unit <String> 使用单位 个人:'个人' 组织:'组织-组织名' default='个人'
     *   - startTime <Date> 开始时间 default=new Date()
     *   - endTime <Date> 结束时间 default=new Date()
     *   - reason <String> 未通过原因 default='该时间段已经有人使用该功能房'
     * @return <Promise>
     */
    reject(to, option) {
      option = _.assign({
        name: '',
        applyID: '',
        room: '',
        unit:'个人',
        startTime: new Date(),
        endTime: new Date(),
        reason: '该时间段已经有人使用该功能房'
      }, option)
      let title = '[SAC - 您的申请未通过审批]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
        您好！十分抱歉，<b>您的申请未能通过审批</b>。<br/>
        原因：${option.reason}。
        <p>
        <table width="870px" border="1" cellpadding="20">
          <tr>
            <th>预约号</th>
            <th>功能房</th>
            <th>使用单位</th>
            <th>使用日期</th>
            <th>开始时间</th>
            <th>结束时间</th>
            <th>审批状态</th>
          </tr>
          <tr>
            <td align="center" width="60px">${option.applyID}</td>
            <td align="center" width="120px">${option.room}</td>
            <td align="center" width="200px">${option.unit}</td>
            <td align="center" width="150px">${moment(option.usedate).format('YYYY-MM-DD')}</td>
            <td align="center" width="80px">${option.startTime}</td>
            <td align="center" width="80px">${option.endTime}</td>
            <td align="center" width="80px">未通过</td>
          </tr>
        </table>
        </p>
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
     * 忘记密码 重置邮件
     * @method apply
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - newpwd <String> 新密码 default=''
     * @return <Promise>
     */
    password(to, option) {
      option = _.assign({
        name: '',
        newpwd: ''
      }, option)
      let title = '[SAC - 您的密码已重置]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
          您的密码已经重置，新的密码为 <b>${option.newpwd}</b>，请妥善保管。
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
     * 违规使用的通知邮件
     * @method apply
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - applyID <String> 预约号 default=''
     *   - room <String> 房间名 default=''
     *   - unit <String> 使用单位 个人:'个人' 组织:'组织-组织名' default='个人'
     *   - usedate <String> 使用日期 default=''
     *   - startTime <Date> 开始时间 default=''
     *   - endTime <Date> 结束时间 default=''
     *   - detail <String> 违规详情 default=''
     * @return <Promise>
     */
    breakRule(to, option) {
      // console.log(to)
      // console.log(option)
      option = _.assign({
        name: '',
        applyID: '',
        room: '',
        unit:'',
        usedate: '',
        startTime: '',
        endTime: '',
        detail: ''
      }, option)
      let title = '[SAC - 违规使用通知]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
          您好！您在使用功能房过程中出现了违规。<br/>
          违规行为：<b>${option.detail}<b/>
          <p>
          <table width="870px" border="1" cellpadding="20">
            <tr>
              <th>预约号</th>
              <th>功能房</th>
              <th>使用单位</th>
              <th>使用日期</th>
              <th>开始时间</th>
              <th>结束时间</th>
              <th>状态</th>
            </tr>
            <tr>
              <td align="center" width="60px">${option.applyID}</td>
              <td align="center" width="120px">${option.room}</td>
              <td align="center" width="200px">${option.unit}</td>
              <td align="center" width="150px">${moment(option.usedate).format('YYYY-MM-DD')}</td>
              <td align="center" width="80px">${option.startTime}</td>
              <td align="center" width="80px">${option.endTime}</td>
              <td align="center" width="80px">违规使用</td>
            </tr>
          </table>
          </p>
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`

      return sendMail(to, title, content)
    },
    /*
     * 发送长期使用账号,
     * @method sendLongterm
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - sid <String> 账号 default=''
     * @return <Promise>
     */
    sendLongterm(to, option) {
      option = _.assign({
        name: '',
        sid: '',
      }, option)
      option.sid = String(option.sid)
      let title = '[长期使用账号发布]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
        您好！<br/><br/>
        贵社团的长期使用账号为<big>${option.sid}</big>,如为首次申请长期使用，则密码与账号一致，
        请及时登录修改密码，否则仍维持原密码；此长期帐号上只可取消长期使用记录，不可主动添加，最迟取
        消时间为使用时间的24小时前，其他具体情况敬请关注至善学生活动中心管理制度。长期使用的全部记录
        均已添加至申请列表，到达使用日期临近会自动审批为通过，请登录长期使用帐号检查申请记录是否存在
        问题；如有，请及时与我们联系。由于开学初部分房间已被其他同学提前申请，长期使用的具体审批情况
        请以系统中查询到的结果为准。若存在由不知此情况而导致的违规等任何后果，将由贵社团自行承担，造
        成的不便我们深表歉意。<br/><br/>

        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
     * 临时闭馆,发送房间取消通知
     * @method sudden_cancel
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名
     *   - reason <String> 闭馆原因
     *   - usedate <String> 姓名 房间申请使用日期
     *   - room <String> 账号 房间名字
     *   - applyID <String> 房间预约号
     * @return <Promise>
     */
    sudden_cancel(to, option) {
      option = _.assign({
        name: '',
        reason: '',
        usedate: '',
        room: '',
        applyID: '',
      }, option)
      let title = '[临时闭馆通知]'
      let content =
        `亲爱的${option.name}同学：<br/>
        <blockquote>
        您好！<br/><br/>
        由于<big>${option.reason}</big>，至善学生活动中心将会暂停功能房使用。因此您的预约号为${option.applyID}的预约：
        <big>${moment(option.usedate).format('YYYY-MM-DD')}</big>的<big>${option.room}</big>将被取消，为您造成的不便
        我们深表歉意。<br/><br/>

        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
    /*
   * 发送每日长期违规数量给外联组负责人
   * @method outreach_master
   * @param to <String> 收件人
   * @param option <Object> 选项
   *   - longterm_blockedNumber <Number> 长期违规通过数量 default=0
   * @return <Promise>
   */
  outreach_master(to, option) {
    option = _.assign({
      longterm_blockedNumber: 0,
    }, option)
    let title = '[SAC-今日长期违规数量]'
    let content =
      `致外联组负责人:<br/>
      今日长期违规数量为 ${option.longterm_blockedNumber}。具体哪些长期使用社团的违规情况请查看今日的违规邮件。<br/>
      中山大学至善学生活动中心服务器<br/>
      `
    return sendMail(to, title, content)
  },
  /*
     * 每日长期违规具体情况，送达外联组负责人
     * @method apply
     * @param to <String> 收件人
     * @param option <Object> 选项
     *   - name <String> 姓名 default=''
     *   - applyID <String> 预约号 default=''
     *   - room <String> 房间名 default=''
     *   - unit <String> 使用单位 个人:'个人' 组织:'组织-组织名' default='个人'
     *   - usedate <String> 使用日期 default=''
     *   - startTime <Date> 开始时间 default=''
     *   - endTime <Date> 结束时间 default=''
     *   - detail <String> 违规详情 default=''
     * @return <Promise>
     */
    longterm_breakRule(to, option) {
      option = _.assign({
        name: '',
        applyID: '',
        room: '',
        unit: '',
        usedate: '',
        startTime: '',
        endTime: '',
        detail: ''
      }, option)
      let title = '[SAC - 今日长期违规具体情况]'
      let content =
        `亲爱的外联组负责人：<br/>
        <blockquote>
          您好！今日${option.name}在使用功能房过程中出现了违规。<br/>
          违规行为：<b>${option.detail}<b/>
          <p>
          <table width="870px" border="1" cellpadding="20">
            <tr>
              <th>预约号</th>
              <th>功能房</th>
              <th>使用单位</th>
              <th>使用日期</th>
              <th>开始时间</th>
              <th>结束时间</th>
              <th>状态</th>
            </tr>
            <tr>
              <td align="center" width="60px">${option.applyID}</td>
              <td align="center" width="120px">${option.room}</td>
              <td align="center" width="200px">${option.unit}</td>
              <td align="center" width="150px">${moment(option.usedate).format('YYYY-MM-DD')}</td>
              <td align="center" width="80px">${option.startTime}</td>
              <td align="center" width="80px">${option.endTime}</td>
              <td align="center" width="80px">违规使用</td>
            </tr>
          </table>
          </p>
        </blockquote>
        <p align="right">中山大学至善学生活动中心<br/>请勿回复</p>`
      return sendMail(to, title, content)
    },
}
