/*
 * @author: Jianfei2333
 * @email: xjf999999@hotmail.com
 * @function: response模板信息
 * 字段description为描述信息(如失败原因描述，错误信息)
 * 当状态为SUCCESS时，可选择添加字段content(默认为空),携带数据载荷
 */

exports.success = (res, description, content = null) => {
  var status = 'SUCCESS'
  return res.json({
    status,
    description,
    content
  })
}

exports.fail = (res, description) => {
  var status = 'FAIL'
  return res.json({
    status,
    description
  })
}

exports.error = (res, description) => {
  var status = 'ERROR'
  console.log(description)
  return res.json({
    status,
    description
  })
}
