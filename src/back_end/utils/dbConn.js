const config = require('../config/config');
const Promise = require('bluebird');
const mysql = require('mysql');

Promise.promisifyAll(require('mysql/lib/Connection').prototype);
Promise.promisifyAll(require('mysql/lib/Pool').prototype);

let dbconf = Object.assign({ connectionLimit: 10 }, config.db_config);
let pool = mysql.createPool(dbconf);

const getConnection = () => {
	return pool
			.getConnectionAsync()
			.disposer(connection => {
				connection.release();
			});
};

//数据库连接在查询数据后会自动将连接返回连接池；
exports.queryDb = (sql, values) => {
 	return Promise.using(getConnection(), connection => {
	      return connection.queryAsync(sql,values);//此处连接未返回连接池
	    });
			//返回对象的下一个then会自动执行disposer,将连接返回连接池；
};

//***************************************************************
//以下为定时执行脚本的专用数据库查询接口，使得连接能够自行断开，
//程序能够正常结束。
const getConnection_for_scripts = () => {
	return pool
			.getConnectionAsync()
			.disposer(connection => {
				connection.destroy();
			});
};

exports.queryDb_for_scripts = (sql, values) => {
 	return Promise.using(getConnection_for_scripts(), connection => {
	      return connection.queryAsync(sql,values);
	    });
};
//***************************************************************

exports.pool = pool;
