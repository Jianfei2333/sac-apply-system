var gulp = require('gulp');
var watch = require('gulp-watch');
var run = require('gulp-run');

var express = require('gulp-express');
var changed = require('gulp-changed');
var open = require('gulp-open');

/*
full-stack development implementation:

The back-end source files will be watched by gulp itself,
while the front-end source files will be watched by the
Webpack-dev-server. Thus, we have two web servers running
when we are developing the project. To deal with the
cross-origin problem, we simply set a proxy in the
Webpack-dev-server so that it can relay all the requests
to our express server.

When back-end source files change,
the server will restart and the browser will not.
Vice versa when front-end source files change.
*/

gulp.task('full-stack', ['transmit-file'], function() {
  express.run(['./dist/back_end/bin/www']);
  run('npm run dev').exec();
  var be_watcher = gulp.watch('./src/back_end/**', ['restart-server']);

});

gulp.task('restart-server', ['transmit-file'], function() {
  express.stop();
  express.run(['./dist/back_end/bin/www']);

});


gulp.task('transmit-file', function() {
  //simply transmit the files, can add more processing methods in the future.
  return gulp.src('./src/back_end/**')
    .pipe(changed('./dist/back_end/'))
    .pipe(gulp.dest('./dist/back_end/'));

});
